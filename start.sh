docker start grafana-storage || docker run -d -v /var/lib/graphana_storage --name grafana-storage busybox:latest
docker start grafana || docker run -d -p 3000:3000 --volumes-from grafana-storage --name grafana grafana/grafana
docker start influxdb || docker run -d -p 8086:8086 -v influxdb:/var/lib/influxdb --name influxdb influxdb
kaitai-struct-compiler -t python gsbc.ksy
python3 listener.py
