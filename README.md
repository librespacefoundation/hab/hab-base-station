# HAB Base station software

This is the base station reception and vusalization software.

It will demodulate data from an SDR, and display it using Grafana through InfluxDB

## Intallation

KaiTai and InfuxDB connector

```
pip3 install kaitaistruct influxdb
```

Docker

```
sudo apt install docker-ce
```

GNU Radio

```
sudo apt install gnuradio
```


gr-satnogs

```bash
git clone https://gitlab.com/librespacefoundation/satnogs/gr-satnogs.git
cd gr-satnogs
git reset 571508b7c08bfe66c733b5dda2e5137cdc48d99f --hard
mkdir build; cd build
cmake ..
make -j $(nproc --all)
sudo make install
sudo ldconfig
```

For details visit https://gitlab.com/librespacefoundation/satnogs/gr-satnogs

## Run

`./start.sh`

Open http://localhost:3000/

Import HAB.json in grafana